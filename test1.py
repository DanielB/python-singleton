# Author: Daniel Basulto
#Singleton version 1

class TestSIngleton(object):

	"""docstring for TestSIngleton"""
	
	_instance = None

	def __new__(self):
		if not self._instance:
			self._instance = super(TestSIngleton, self).__new__(self)
			self.value = 10
		return self._instance

def main():
	prueba = TestSIngleton()
	print(prueba.value)
	# >> 10

	# Change the value
	prueba2 = TestSIngleton()
	print(prueba2.value)
	# >> 10


if __name__ == '__main__':
	main()
		