# Author: Daniel Basulto
# Singleton version 2

def singleton(myClass):
	
	instances = {}
	def getInstance(*args, **kwargs):
		if myClass not in instances:
			instances[myClass] = myClass(*args, **kwargs)
		return instances[myClass]

	return getInstance

@singleton
class TestSingleton2(object):

 	"""docstring for TestSingleton2"""

 	pass

def main():

	object1 = TestSingleton2()
	print(object1)
	# >> <__main__.TestSingleton2 object at 0x7f558abb0908>

	object2 = TestSingleton2()
	print(object2)
	# >> <__main__.TestSingleton2 object at 0x7f558abb0908>


if __name__ == '__main__':
	main()